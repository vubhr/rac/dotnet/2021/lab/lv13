#nullable disable
using Microsoft.AspNetCore.Mvc;

namespace ShowsAPI.Controllers {
  [Route("api/[controller]")]
  [ApiController]
  public class ShowsController : ControllerBase {
    private readonly IShowService ss;
    private readonly IEpisodeService es;

    public ShowsController(IShowService showService, IEpisodeService episodeService) {
      ss = showService;
      es = episodeService;
    }

    // GET: api/Shows
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Show>>> GetShow() {
      return Ok(await ss.All());
    }

    // GET: api/Shows/5
    [HttpGet("{id}")]
    public async Task<ActionResult<Show>> GetShow(int id) {
      var show = await ss.Get(id);

      if (show == null) {
        return NotFound();
      }

      return show;
    }

    // GET: api/Shows/5/Episodes
    [HttpGet("{id}/Episodes")]
    public async Task<ActionResult<IEnumerable<Episode>>> GetShowEpisodes(int id) {
      var show = await ss.Get(id);

      if (show != null) {
        return Ok(await es.All(id));
      }

      return NotFound();
    }

    // GET: api/Shows/5/Episodes/3
    [HttpGet("{showId}/Episodes/{episodeId}")]
    public async Task<ActionResult<IEnumerable<Episode>>> GetShowEpisode(int showId, int episodeId) {
      var show = await ss.Get(showId);

      if (show != null) {
        var episode = await es.Get(episodeId);
        if (episode != null) {
          return Ok(episode);
        }
        return NotFound();
      }

      return NotFound();
    }

    // PUT: api/Shows/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutShow(int id, Show show) {
      if (id != show.Id) {
        return BadRequest();
      }

      if (await ss.Update(show)) {
        return NoContent();
      }

      return StatusCode(StatusCodes.Status500InternalServerError);
    }

    // PUT: api/Shows/5/Episodes/3
    [HttpPut("{showId}/Episodes/{episodeId}")]
    public async Task<IActionResult> PutShowEpisode(int showId, int episodeId, Episode episode) {
      if (episodeId != episode.Id) {
        return BadRequest();
      }

      var show = await ss.Get(showId);
      if (show != null) {
        if (await es.Update(episode)) {
          return NoContent();
        } else {
          return StatusCode(StatusCodes.Status500InternalServerError);
        }
      }

      return NotFound();
    }
    // POST: api/Shows
    [HttpPost]
    public async Task<ActionResult<Show>> PostShow(Show show) {
      await ss.Insert(show);

      return CreatedAtAction("GetShow", new { id = show.Id }, show);
    }

    // POST: api/Shows/5/Episodes
    [HttpPost("{id}/Episodes")]
    public async Task<ActionResult<Episode>> PostShowEpisode(int id, Episode episode) {
      //if (id != episode.ShowId) {
      //  return BadRequest();
      //}

      var show = await ss.Get(id);
      if (show != null) {
        await es.Insert(episode);
        return CreatedAtAction("GetShowEpisode", new { showId = id, episodeId = episode.Id }, episode);
      }

      return NotFound();
    }

    // DELETE: api/Shows/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteShow(int? id) {
      if (id == null) return NotFound();

      Show show;
      if ((show = await ss.Get(id)) != null) {
        await ss.Delete(show.Id);
        return NoContent();
      } else {
        return NotFound();
      }
    }

    // DELETE: api/Shows/5/Episodes/3
    [HttpDelete("{showId}/Episodes/{episodeId}")]
    public async Task<IActionResult> DeleteShowEpisode(int showId, int episodeId) {
      var show = await ss.Get(showId);
      if (show != null) {
        await es.Delete(episodeId);
        return NoContent();
      }

      return NotFound();
    }
  }
}
