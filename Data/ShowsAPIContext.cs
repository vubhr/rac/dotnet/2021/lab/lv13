#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ShowsAPI.Models;

public class ShowsAPIContext : DbContext {
  public ShowsAPIContext(DbContextOptions<ShowsAPIContext> options)
      : base(options) {
  }

  public DbSet<ShowsAPI.Models.Show> Shows { get; set; }
  public DbSet<ShowsAPI.Models.Episode> Episodes { get; set; }
}
