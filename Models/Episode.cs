namespace ShowsAPI.Models;

public class Episode {
  public int Id { get; set; }

  public int ShowId { get; set; }
//  public Show? Show { get; set; }

  public int Season { get; set; }

  public int NumberInSeason { get; set; }

  public string? Title { get; set; }
}