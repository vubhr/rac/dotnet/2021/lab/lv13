using System.Collections;

namespace ShowsAPI.Models;

public interface IEpisodeService {
  Task<IEnumerable> All(int showId);

  Task<Episode?> Get(int? id);

  Task<int> Insert(Episode? episode);

  Task<bool> Update(Episode? episode);

  Task<int> Delete(int id);
}