using System.Collections;

namespace ShowsAPI.Models;

public interface IShowService {
  Task<IEnumerable> All();

  Task<Show?> Get(int? id);

  Task<int> Insert(Show? show);

  Task<bool> Update(Show? show);

  Task<int> Delete(int id);
}