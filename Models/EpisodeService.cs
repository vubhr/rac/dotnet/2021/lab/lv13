using System.Collections;
using Microsoft.EntityFrameworkCore;

namespace ShowsAPI.Models;

public class EpisodeService : IEpisodeService {
  public EpisodeService(ShowsAPIContext context) {
    Db = context;
  }

  public async Task<IEnumerable> All(int showId) {
    return await Db.Episodes.Where(e => e.ShowId == showId).ToListAsync();
  }

  public async Task<Episode?> Get(int? id) {
    return await Db.Episodes.FirstOrDefaultAsync(m => m.Id == id);
  }

  public async Task<int> Insert(Episode? episode) {
    if (episode != null) {
      Db.Add(episode);
      return await Db.SaveChangesAsync();
    }
    return -1;
  }

  public async Task<bool> Update(Episode? episode) {
    if (episode != null) {
      try {
        Db.Update(episode);
        await Db.SaveChangesAsync();
        return true;
      } catch (DbUpdateConcurrencyException) {
        return false;
      }
    }
    return false;
  }

  public async Task<int> Delete(int id) {
    var episode = await Get(id);
    if (episode != null) {
      Db.Episodes.Remove(episode);
      return await Db.SaveChangesAsync();
    }
    return 0;
  }

  public ShowsAPIContext Db { get; }
}