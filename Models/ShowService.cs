using System.Collections;
using Microsoft.EntityFrameworkCore;

namespace ShowsAPI.Models;

public class ShowService : IShowService {
  public ShowService(ShowsAPIContext context) {
    Db = context;
  }

  public async Task<IEnumerable> All() {
    return await Db.Shows.ToListAsync();
  }

  public async Task<Show?> Get(int? id) {
    return await Db.Shows.FirstOrDefaultAsync(m => m.Id == id);
  }

  public async Task<int> Insert(Show? show) {
    if (show != null) {
      Db.Add(show);
      return await Db.SaveChangesAsync();
    }
    return -1;
  }

  public async Task<bool> Update(Show? show) {
    if (show != null) {
      try {
        Db.Update(show);
        await Db.SaveChangesAsync();
        return true;
      } catch (DbUpdateConcurrencyException) {
        return false;
      }
    }
    return false;
  }

  public async Task<int> Delete(int id) {
    var show = await Get(id);
    if (show != null) {
      Db.Shows.Remove(show);
      return await Db.SaveChangesAsync();
    }
    return 0;
  }

  public ShowsAPIContext Db { get; }
}